#ifndef CATEGORY_H
#define CATEGORY_H
 
	// Entity/scene node category, used to dispatch commands
	namespace Category
	{
		enum Type
		{
			None				= 0,
			SceneAirLayer		= 1 << 0,
			Cannon				= 1 << 1,
			Clown				= 1 << 2,
			Floor				= 1 << 3,
			Wall				= 1 << 4,
			Net					= 1 << 5,
			ParticleSystem		= 1 << 7,
			SoundEffect			= 1 << 8,

			// composite catagories
			AllActors			= Cannon | Clown,
		};
	}

#endif 
