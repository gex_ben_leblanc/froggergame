#ifndef PARTICLENODE_H
#define PARTICLENODE_H

#include "SceneNode.h"
#include "ResourceIdentifiers.h"
#include "Particle.h"

#include <SFML/Graphics/VertexArray.hpp>

#include <deque>


class ParticleNode : public gex::SceneNode
{
public:
	ParticleNode(Particle::Type type, const gex::TextureMgr& textures);

	void					addParticle(sf::Vector2f position);
	Particle::Type			getParticleType() const;
	virtual unsigned int	getCategory() const;


private:
	virtual void			updateCurrent(sf::Time dt, gex::CommandQueue& commands);
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

	void					addVertex(float worldX, float worldY, float texCoordX, float texCoordY, const sf::Color& color) const;
	void					computeVertices() const;


private:
	std::deque<Particle>	_particles;
	const sf::Texture&		_texture;
	Particle::Type			_type;

	mutable sf::VertexArray	_vertexArray;
	mutable bool			_needsVertexUpdate;
};

#endif 
