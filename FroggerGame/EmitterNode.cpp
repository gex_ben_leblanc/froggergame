#include "EmitterNode.h"
#include "ParticleNode.h"
#include "CommandQueue.h"
#include "Command.h"
#include "Category.h"

EmitterNode::EmitterNode(Particle::Type type) : 
SceneNode(),
_accumulatedTime(sf::Time::Zero),
_type(type),
_particleSystem(nullptr)
{
}

void EmitterNode::updateCurrent(sf::Time dt, gex::CommandQueue& commands)
{
	if (_particleSystem)
	{
		emitParticles(dt);
	}
	else
	{
		// Find particle node with the same type as emitter node
		auto finder = [this](ParticleNode& container, sf::Time)
		{
			if (container.getParticleType() == _type)
				_particleSystem = &container;
		};

		gex::Command command;
		command.category = Category::ParticleSystem;
		command.action = gex::derivedAction<ParticleNode>(finder);

		commands.push(command);
	}
}

void EmitterNode::emitParticles(sf::Time dt)
{
	const float emissionRate = 30.f;
	const sf::Time interval = sf::seconds(1.f) / emissionRate;

	_accumulatedTime += dt;

	while (_accumulatedTime > interval)
	{
		_accumulatedTime -= interval;
		_particleSystem->addParticle(getWorldPosition());
	}
}
