#ifndef GEXSTATE_H
#define GEXSTATE_H

#include "State.h"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class GexState : public gex::State
{
public:
	GexState(gex::StateStack& stack, gex::State::Context context);
	~GexState();
	
	virtual void		draw();
	virtual bool		update(sf::Time dt);
	virtual bool		handleEvent(const sf::Event& event);


private:
	sf::Sprite			_backgroundSprite;
	sf::Text			_pausedText;
	sf::Text			_instructionText;
};

#endif 