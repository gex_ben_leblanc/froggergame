#include "Application.h"
#include "Utility.h"
#include "State.h"
#include "TitleState.h"
#include "GameState.h"
#include "GameOverState.h"
#include "MenuState.h"
#include "PauseState.h"
#include "GexState.h"
#include "StateID.h"



const sf::Time Application::_timePerFrame = sf::seconds(1.f / 60.f);

Application::Application() :
_window(sf::VideoMode(1024, 768), "States", sf::Style::Close),
_textures(),
_fonts(),
_player(),
_music(),
_stateStack(gex::State::Context(_window, _textures, _fonts, _player, _music)),
_statisticsText(),
_statisticsUpdateTime(),
_statisticsNumFrames(0)
{
	_window.setKeyRepeatEnabled(false);

	_window.setVerticalSyncEnabled(false);
	_window.setFramerateLimit(60);

	_fonts.load(FontID::Main, "../Media/arial.ttf");
	_textures.load(TextureID::TitleScreen, "../Media/Textures/GEX.png");
	_textures.load(TextureID::Face, "../Media/Textures/face.png");

	_statisticsText.setFont(_fonts.get(FontID::Main));
	_statisticsText.setPosition(5.f, 5.f);
	_statisticsText.setCharacterSize(10u);

	registerStates();
	_stateStack.pushState(StateID::Title);
}

void Application::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while (_window.isOpen())
	{
		sf::Time dt = clock.restart();
		timeSinceLastUpdate += dt;
		while (timeSinceLastUpdate > _timePerFrame)
		{
			timeSinceLastUpdate -= _timePerFrame;

			processInput();
			update(_timePerFrame);

			// Check inside this loop, because stack might be empty before update() call
			if (_stateStack.isEmpty())
				_window.close();
		}

		updateStatistics(dt);
		render();
	}
}

void Application::processInput()
{
	sf::Event event;
	while (_window.pollEvent(event))
	{
		_stateStack.handleEvent(event);

		if (event.type == sf::Event::Closed)
			_window.close();
	}
}

void Application::update(sf::Time dt)
{
	_stateStack.update(dt);
}

void Application::render()
{
	_window.clear();

	_stateStack.draw();

	_window.setView(_window.getDefaultView());
	_window.draw(_statisticsText);

	_window.display();
}

void Application::updateStatistics(sf::Time dt)
{
	_statisticsUpdateTime += dt;
	_statisticsNumFrames += 1;
	if (_statisticsUpdateTime >= sf::seconds(1.0f))
	{
		_statisticsText.setString("FPS: " + toString(_statisticsNumFrames));

		_statisticsUpdateTime -= sf::seconds(1.0f);
		_statisticsNumFrames = 0;
	}
}

void Application::registerStates()
{
	_stateStack.registerState<TitleState>(StateID::Title);
	_stateStack.registerState<MenuState>(StateID::Menu);
	_stateStack.registerState<GameState>(StateID::Game);
	_stateStack.registerState<PauseState>(StateID::Pause);
	_stateStack.registerState<GexState>(StateID::Gex);
	_stateStack.registerState<GameOverState>(StateID::GameOver);

}
