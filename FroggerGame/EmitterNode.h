#ifndef EMITTERNODE_H
#define EMITTERNODE_H

#include "SceneNode.h"
#include "Particle.h"


class ParticleNode;

class EmitterNode : public gex::SceneNode
{
public:
	explicit				EmitterNode(Particle::Type type);


private:
	virtual void			updateCurrent(sf::Time dt, gex::CommandQueue& commands);

	void					emitParticles(sf::Time dt);


private:
	sf::Time				_accumulatedTime;
	Particle::Type			_type;
	ParticleNode*			_particleSystem;
};

#endif  
