#include "GexState.h"
#include "Utility.h"
#include "ResourceMgr.h"
#include "ResourceIdentifiers.h"
#include "StateID.h"
#include "MusicPlayer.h"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>


GexState::GexState(gex::StateStack& stack, gex::State::Context context) :
State(stack, context),
_backgroundSprite(),
_pausedText(),
_instructionText()
{
	// Pause game music
	context.music->setPaused(true);

	sf::Font& font = context.fonts->get(FontID::Main);
	sf::Vector2f viewSize = context.window->getView().getSize();

	_backgroundSprite.setTexture(context.textures->get(TextureID::Face));
	centerOrigin(_backgroundSprite);
	_backgroundSprite.setPosition(0.5f * viewSize.x, 0.5f * viewSize.y);
	_backgroundSprite.setColor(sf::Color(255, 255, 255, 100));

	_pausedText.setFont(font);
	_pausedText.setString("Game Paused\n   GEX STATE");
	_pausedText.setCharacterSize(70);
	centerOrigin(_pausedText);
	_pausedText.setPosition(0.5f * viewSize.x, 0.4f * viewSize.y);

	_instructionText.setFont(font);
	_instructionText.setString("(Press Backspace to return to the main menu)");
	centerOrigin(_instructionText);
	_instructionText.setPosition(0.5f * viewSize.x, 0.6f * viewSize.y);
}

GexState::~GexState()
{
	// Unpause game music
	getContext().music->setPaused(false);
}

void GexState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(150, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(_backgroundSprite);
	window.draw(_pausedText);
	window.draw(_instructionText);
}

bool GexState::update(sf::Time)
{
	return false;
}

bool GexState::handleEvent(const sf::Event& event)
{
	if (event.type != sf::Event::KeyPressed)
		return false;

	if (event.key.code == sf::Keyboard::G)
	{
		// Escape G, remove itself to return to the game
		requestStackPop();
	}

	if (event.key.code == sf::Keyboard::BackSpace)
	{
		// Escape pressed, remove itself to return to the game
		requestStateClear();
		requestStackPush(StateID::Menu);
	}

	return false;
}