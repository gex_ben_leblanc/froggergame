#include "ParticleNode.h"
#include "DataTables.h"
#include "ResourceMgr.h"
#include "Category.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <algorithm>


namespace
{
	const std::map<Particle::Type, ParticleData> Table = initializeParticleData();
}

ParticleNode::ParticleNode(Particle::Type type, const gex::TextureMgr& textures) : 
SceneNode(),
_particles(),
_texture(textures.get(TextureID::Particle)),
_type(type),
_vertexArray(sf::Quads),
_needsVertexUpdate(true)
{
}

void ParticleNode::addParticle(sf::Vector2f position)
{
	Particle particle;
	particle.position = position;
	particle.color = Table.at(_type).color;
	particle.lifetime = Table.at(_type).lifetime;

	_particles.push_back(particle);
}

Particle::Type ParticleNode::getParticleType() const
{
	return _type;
}

unsigned int ParticleNode::getCategory() const
{
	return Category::ParticleSystem;
}

void ParticleNode::updateCurrent(sf::Time dt, gex::CommandQueue&)
{
	// Remove expired particles at beginning
	while (!_particles.empty() && _particles.front().lifetime <= sf::Time::Zero)
		_particles.pop_front();

	// Decrease lifetime of existing particles
	for (Particle& particle : _particles)
		particle.lifetime -= dt;

	_needsVertexUpdate = true;
}

void ParticleNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (_needsVertexUpdate)
	{
		computeVertices();
		_needsVertexUpdate = false;
	}

	// Apply particle texture
	states.texture = &_texture;

	// Draw vertices
	target.draw(_vertexArray, states);
}

void ParticleNode::addVertex(float worldX, float worldY, float texCoordX, float texCoordY, const sf::Color& color) const
{
	sf::Vertex vertex;
	vertex.position = sf::Vector2f(worldX, worldY);
	vertex.texCoords = sf::Vector2f(texCoordX, texCoordY);
	vertex.color = color;

	_vertexArray.append(vertex);
}

void ParticleNode::computeVertices() const
{
	sf::Vector2f size(_texture.getSize());
	sf::Vector2f half = size / 2.f;

	// Refill vertex array
	_vertexArray.clear();
	for (const Particle& particle : _particles)
	{
		sf::Vector2f pos = particle.position;
		sf::Color color = particle.color;

		float ratio = particle.lifetime.asSeconds() / Table.at(_type).lifetime.asSeconds();
		color.a = static_cast<sf::Uint8>(255 * std::max(ratio, 0.f));  // fade alpha over time

		addVertex(pos.x - half.x, pos.y - half.y, 0.f, 0.f, color);
		addVertex(pos.x + half.x, pos.y - half.y, size.x, 0.f, color);
		addVertex(pos.x + half.x, pos.y + half.y, size.x, size.y, color);
		addVertex(pos.x - half.x, pos.y + half.y, 0.f, size.y, color);
	}
}
