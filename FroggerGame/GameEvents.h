#ifndef EVENTS_H
#define EVENTS_H

enum class GameEvent {
	Load,
	Fire,
	Land,
	Crash,
	Over,
};

#endif