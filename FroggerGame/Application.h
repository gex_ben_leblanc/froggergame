#ifndef GEX_APPLICATION_H
#define GEX_APPLICATION_H

#include "ResourceMgr.h"
#include "ResourceIdentifiers.h"
#include "Player.h"
#include "StateStack.h"
#include "MusicPlayer.h"

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>

class Application
{
public:
	Application();
	void					run();


private:
	void					processInput();
	void					update(sf::Time dt);
	void					render();

	void					updateStatistics(sf::Time dt);
	void					registerStates();


private:
	static const sf::Time	_timePerFrame;

	////////////////////////////////////
	// Context 
	////////////////////////////////////
	sf::RenderWindow		_window;
	gex::TextureMgr			_textures;
	gex::FontMgr			_fonts;
	Player					_player;
	gex::MusicPlayer		_music;
	////////////////////////////////////


	gex::StateStack			_stateStack;

	sf::Text				_statisticsText;
	sf::Time				_statisticsUpdateTime;
	std::size_t				_statisticsNumFrames;
};

#endif 
