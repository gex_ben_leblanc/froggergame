#ifndef STATE_ID_H
#define STATE_ID_H

enum class StateID
{
	None,
	Title,
	Menu,
	Game,
	Loading,
	Pause,
	Gex,
	GameOver,
};

#endif