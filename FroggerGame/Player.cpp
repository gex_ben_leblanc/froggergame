

// game
#include "Player.h"
#include "Category.h"

//gex
#include "CommandQueue.h"
#include "Command.h"

// std
#include <map>
#include <string>
#include <algorithm>
#include <iostream>

Player::Player() :
_currentGameStatus(GameStatus::Go)
{
	// Set initial key bindings
	// key -> action
	_keyBinding[sf::Keyboard::Left] = Action::TiltUp;
	_keyBinding[sf::Keyboard::Right] = Action::TiltDown;
	_keyBinding[sf::Keyboard::Up] = Action::TiltUp;
	_keyBinding[sf::Keyboard::Down] = Action::TiltDown;

	_keyBinding[sf::Keyboard::Space] = Action::Fire;
	_keyBinding[sf::Keyboard::Return] = Action::Load;
	

	// Set initial action bindings
	// action -> command object 
	initializeActions();

}

void Player::setGameStatus(GameStatus status)
{
	// 
	gex::Command next;
	next.category = Category::Clown;
	//next.action = gex::derivedAction<Clown>([](Clown& c, sf::Time dt){c.reset(); });
	//commands.puxh(next)]
	_currentGameStatus = status;
}


void Player::handleEvent(const sf::Event& event, gex::CommandQueue& commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		// Check if pressed key appears in key binding, trigger command if so
		auto found = _keyBinding.find(event.key.code);
		if (found != _keyBinding.end() && !isRealtimeAction(found->second))
			commands.push(_actionBinding[found->second]);
	}
}

void Player::handleRealtimeInput(gex::CommandQueue& commands)
{
	// Traverse all assigned keys and check if they are pressed
	for (auto pair : _keyBinding)
	{
		// If key is pressed, lookup action and trigger corresponding command
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
			commands.push(_actionBinding[pair.second]);
	}
}

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
	// Remove all keys that already map to action
	for (auto itr = _keyBinding.begin(); itr != _keyBinding.end();)
	{
		if (itr->second == action)
			_keyBinding.erase(itr++);
		else
			++itr;
	}

	// Insert new binding
	_keyBinding[key] = action;
}

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
	for (auto pair : _keyBinding)
	{
		if (pair.second == action)
			return pair.first;
	}

	return sf::Keyboard::Unknown;
}

void Player::initializeActions()
{
	const float playerSpeed = 200.f;
	/*
	// set cmd.action
	_actionBinding[Action::TiltUp].action = gex::derivedAction<Cannon>([](Cannon& c, sf::Time dt){c.tilt(-0.5); });
	_actionBinding[Action::TiltUp].category = Category::Cannon;
	_actionBinding[Action::TiltDown].action = gex::derivedAction<Cannon>([](Cannon& c, sf::Time dt){c.tilt(0.5); });
	_actionBinding[Action::TiltDown].category = Category::Cannon;

	_actionBinding[Action::Fire].action = gex::derivedAction<Cannon>([](Cannon& c, sf::Time dt){c.fire(); });
	_actionBinding[Action::Fire].category = Category::Cannon;

	_actionBinding[Action::Load].action = gex::derivedAction<gex::Entity>([](gex::Entity& c, sf::Time df){c.onEvent(GameEvent::Load); });
	_actionBinding[Action::Load].category = Category::Clown | Category::Cannon | Category::Net;
*/
}

bool Player::isRealtimeAction(Action action)
{
	switch (action)
	{
	case Action::TiltUp:
	case Action::TiltDown:
		return true;
	default:
		return false;
	}
}
