#ifndef RESOURCEIDENTIFIERS_H
#define RESOURCEIDENTIFIERS_H

enum class TextureID
{
	BigTop,
	CannonSheet,
	Clown,
	ClownSheet,
	ClownSplat,
	Net,
	TitleScreen,
	Particle,
	Face,
};

enum class FontID
{
	Main,
};

enum class SoundEffectID
{
	ClownLand,
	ClownBounce,
	ClownSplat,
	CannonFire,
};


// Forward declaration of SFML classes
namespace sf
{
	class Texture;
	class Font;
}

namespace gex
{
	// Forward declaration and a few type definitions
	template <typename Resource, typename Identifier> class ResourceMgr;

	typedef ResourceMgr<sf::Texture, TextureID> TextureMgr;
	typedef ResourceMgr<sf::Font, FontID> FontMgr;
}
#endif 