#ifndef GEX_PLAYER_H
#define GEX_PLAYER_H

#include <SFML/Window/Event.hpp>

#include <map>

#include "Command.h"

// forward declaration
namespace gex{
	class CommandQueue;
}

class Player
{
public:
	enum class Action
	{
		TiltUp,
		TiltDown,
		Fire,
		LaunchMissile,
		Load,
	};

	enum class GameStatus
	{
		Go,
		Landed,
		Crashed,
		Over,
	};


public:
	Player();

	void					handleEvent(const sf::Event& event, gex::CommandQueue& commands);
	void					handleRealtimeInput(gex::CommandQueue& commands);

	void					assignKey(Action action, sf::Keyboard::Key key);
	sf::Keyboard::Key		getAssignedKey(Action action) const;

	void 					setGameStatus(GameStatus status);
	 		

private:
	void					initializeActions();
	static bool				isRealtimeAction(Action action);


private:
	std::map<sf::Keyboard::Key, Action>		_keyBinding;
	std::map<Action, gex::Command>			_actionBinding;
	GameStatus								_currentGameStatus;
};


#endif
