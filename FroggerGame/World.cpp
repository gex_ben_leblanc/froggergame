#include "World.h"
#include "SpriteNode.h"
//#include "Pickup.h"
#include "ParticleNode.h"
#include "Utility.h"
#include "SoundNode.h"

#include "Command.h"
#include "Category.h"

#include <iostream>

// forward declaration
bool matchesCategories(gex::SceneNode::Pair& colliders, Category::Type type1, Category::Type type2);


World::World(sf::RenderWindow& window, gex::FontMgr& fonts) :
_window(window),
_worldView(window.getDefaultView()),
_fonts(fonts),
_textures(),
_sounds(),
_sceneGraph(),
_sceneLayers()

{
	// set up the world
	loadTextures();
	loadSoundEffects(); // Load sound effects used by the game into memory
	buildScene();

}


void World::loadTextures()
{
	_textures.load(TextureID::BigTop, "../Media/Textures/bigtop.png");
	_textures.load(TextureID::CannonSheet, "../Media/Textures/cannon_sprite_sheet.png");
	_textures.load(TextureID::Clown, "../Media/Textures/clown.png");
	_textures.load(TextureID::ClownSheet, "../Media/Textures/clown_sprite_sheet.png");
	_textures.load(TextureID::ClownSplat, "../Media/Textures/clown_crashed.png");
	_textures.load(TextureID::Net, "../Media/Textures/net.png");
	_textures.load(TextureID::Particle, "../Media/Textures/Particle.png");
}

void World::loadSoundEffects() // Load sound effects used by the game into memory
{
	// Load the cannon effects
	_sounds.load(SoundEffectID::CannonFire, "../Media/Sounds/cannon_fire.wav");

	// Load the clown effects
	_sounds.load(SoundEffectID::ClownBounce, "../Media/Sounds/clown_bounce.wav");
	_sounds.load(SoundEffectID::ClownLand, "../Media/Sounds/clown_tada.wav");
	_sounds.load(SoundEffectID::ClownSplat, "../Media/Sounds/clown_smash.wav");
}

gex::CommandQueue& World::getCommandQueue()
{
	return _commandQueue;
}

bool matchesCategories(gex::SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	std::size_t category1 = colliders.first->getCategory();
	std::size_t category2 = colliders.second->getCategory();


	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<gex::SceneNode::Pair> collisionPairs;
	_sceneGraph.checkSceneCollision(_sceneGraph, collisionPairs);

	for (gex::SceneNode::Pair pair : collisionPairs)
	{
		/*//
		// Bounce off wall
		//
		if (matchesCategories(pair, Category::Clown, Category::Wall))
		{
			auto& clown = static_cast<Clown&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			// Collision: Clown bounces off wall
			auto vc = clown.getVelocity();
			vc.x *= -1;
			clown.setVelocity(vc);

			playSound(SoundEffectID::ClownBounce);
		}

		//
		// Splat on floor
		//
		if (matchesCategories(pair, Category::Clown, Category::Floor))
		{
			auto& clown = static_cast<Clown&>(*pair.first);
			auto& floor = static_cast<Wall&>(*pair.second);

			// stops moving
			clown.setVelocity(0.f, 0.f);

			// set the clown just ontop of the floor
			clown.setPosition(clown.getPosition().x,
				floor.getPosition().y
				- (floor.getBoundingRect().height / 2.f)
				- clown.getBoundingRect().height / 2.f);

			gex::Command command;
			command.category = Category::Cannon | Category::Clown | Category::Net;
			command.action = gex::derivedAction<gex::Entity>([](gex::Entity& c, sf::Time df){c.onEvent(GameEvent::Crash); });
			_commandQueue.push(command);

			playSound(SoundEffectID::ClownSplat);

		}

		//
		// Land on net
		//
		if (matchesCategories(pair, Category::Clown, Category::Net))
		{
			auto& clown = static_cast<Clown&>(*pair.first);
			auto& net = static_cast<Net&>(*pair.second);

			clown.setVelocity(0.f, 0.f);

			// check if it slid in from the side
			sf::FloatRect intersection;
			clown.getBoundingRect().intersects(net.getBoundingRect(), intersection);
			if (intersection.height < intersection.width)
			{
				clown.move(0.f, -intersection.height / 2.f);
				gex::Command command;
				command.category = Category::Cannon | Category::Clown | Category::Net;
				command.action = gex::derivedAction<gex::Entity>([](gex::Entity& c, sf::Time df){c.onEvent(GameEvent::Land); });
				_commandQueue.push(command);
				playSound(SoundEffectID::ClownLand);
			}
			else
			{
				float push;
				push = (clown.getPosition().x < net.getPosition().x) ? intersection.width : -intersection.width;
				clown.move(push, 0);
			}

			
			
		}*/
	}
}


void World::update(sf::Time dt)
{

	// send all commands to sceneGraph
	while (!_commandQueue.isEmpty())
		_sceneGraph.onCommand(_commandQueue.pop(), dt);

	handleCollisions();

	//
	// update all objects in world
	//
	_sceneGraph.update(dt, _commandQueue);

	// Remove any sounds that have stopped playing
	_sounds.removeStoppedSounds();
}

void World::draw()
{
	//_window.setView(_worldView);
	_window.draw(_sceneGraph);
}

void World::buildScene()
{
	//
	// set up layers, from Layers enum
	//
	for (int i = 0; i < LayerCount; ++i)
	{
		gex::SceneNode::Ptr layer(new gex::SceneNode());
		_sceneLayers.push_back(layer.get());              // save raw pointer to each layer
		_sceneGraph.attachChild(std::move(layer));
	}


	//
	// set up Background and add to sceneGraph
	//
	std::unique_ptr<gex::SpriteNode> backgroundSprite(new gex::SpriteNode(_textures.get(TextureID::BigTop)));
	_sceneLayers[Background]->attachChild(std::move(backgroundSprite)); // put it in the background layer

	/*
	//
	// Add Cannon
	//
	std::unique_ptr<Cannon> cannon(new Cannon(_textures, _fonts));
	cannon->setPosition(75, 700);
	_sceneLayers[Foreground]->attachChild(std::move(cannon));

	// 
	// add a clown
	//
	std::unique_ptr<Clown> clown(new Clown(_textures, _fonts));
	_theClown = clown.get();
	_sceneLayers[Foreground]->attachChild(std::move(clown));


	// Add Walls
	std::unique_ptr<Wall> wall(new Wall(Category::Type::Wall, 10, 750, { 255, 0, 0, 255 }));

	wall->setPosition(_worldView.getSize().x, _worldView.getSize().y / 2.f);
	_sceneLayers[Foreground]->attachChild(std::move(wall));

	std::unique_ptr<Wall> floor(new Wall(Category::Type::Floor, 1020, 10, { 0, 0, 255, 255 }));
	floor->setPosition(_worldView.getSize().x / 2.f, _worldView.getSize().y - 40);


	// Add Net
	std::unique_ptr<Net> net(new Net(_textures, _fonts));
	float offSet = net->getBoundingRect().height / 2.f + floor->getBoundingRect().height / 2.f;
	net->move(0, -offSet);
	floor->attachChild(std::move(net));

	_sceneLayers[Foreground]->attachChild(std::move(floor));
	*/


	// Add sound effects node
	std::unique_ptr<gex::SoundNode> soundNode(new gex::SoundNode(_sounds, Category::SoundEffect));
	_sceneGraph.attachChild(std::move(soundNode));
}

void World::playSound(SoundEffectID effect)
{
	// Instantiate a new command for the sound node to pick up
	gex::Command command;

	// Set the command's category so that only the SoundEffect subscribers use the command
	command.category = Category::SoundEffect;

	// Set the command's action to play the effect passed in
	command.action = gex::derivedAction<gex::SoundNode>(
		[effect](gex::SoundNode& node, sf::Time)
	{
		node.playSound(effect, sf::Vector2f(0,0));
	});

	// Add the command to the command queue
	_commandQueue.push(command);
}