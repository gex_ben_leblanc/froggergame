#ifndef WORLD_H
#define WORLD_H

#include <vector>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "ResourceIdentifiers.h"
#include "ResourceMgr.h"
#include "SceneNode.h"

#include "CommandQueue.h"
#include "SoundPlayer.h"


// Forward declaration
namespace sf
{
	class RenderWindow;
}



class World : private sf::NonCopyable
{
public:
							World(sf::RenderWindow& window, gex::FontMgr& fonts);

	void					update(sf::Time dt);
	void					draw();

	gex::CommandQueue&		getCommandQueue();


private:
	void 					loadTextures();
	void					loadSoundEffects();
	void					handleCollisions();
	void					buildScene();
	void					playSound(SoundEffectID effect);
	
private:
	enum Layer
	{
		Background,
		Foreground,
		LayerCount
	};

private:
	sf::RenderWindow&				_window;
	sf::View						_worldView;
	gex::TextureMgr					_textures;
	gex::FontMgr&					_fonts;
	gex::SoundPlayer				_sounds;

	gex::SceneNode					_sceneGraph;
	std::vector<gex::SceneNode*>	_sceneLayers;
	gex::CommandQueue				_commandQueue;
	sf::FloatRect					_worldBounds;
	
};


#endif