#ifndef PAUSESTATE_H
#define PAUSESTATE_H

#include "State.h"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class PauseState : public gex::State
{
public:
	PauseState(gex::StateStack& stack, gex::State::Context context);
	~PauseState();

	virtual void		draw();
	virtual bool		update(sf::Time dt);
	virtual bool		handleEvent(const sf::Event& event);


private:
	sf::Sprite			_backgroundSprite;
	sf::Text			_pausedText;
	sf::Text			_instructionText;
};

#endif 