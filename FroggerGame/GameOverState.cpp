#include "GameOverState.h"
#include "Utility.h"
#include "Player.h"
#include "ResourceMgr.h"
#include "ResourceIdentifiers.h"
#include "StateID.h"
#include "MusicPlayer.h"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>


GameOverState::GameOverState(gex::StateStack& stack, gex::State::Context context) : 
State(stack, context),
_gameOverText(),
_elapsedTime(sf::Time::Zero)
{
	// End game music
	context.music->stop();

	sf::Font& font = context.fonts->get(FontID::Main);
	sf::Vector2f windowSize(context.window->getSize());
	_gameOverText.setFont(font);
	_gameOverText.setString("Game over!");

	_gameOverText.setCharacterSize(70);
	centerOrigin(_gameOverText);
	_gameOverText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);
}

void GameOverState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	// Create dark, semitransparent background
	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(_gameOverText);
}

bool GameOverState::update(sf::Time dt)
{
	// Show state for 3 seconds, after return to menu
	_elapsedTime += dt;
	if (_elapsedTime > sf::seconds(3))
	{
		requestStateClear();
		requestStackPush(StateID::Menu);
	}
	return false;
}

bool GameOverState::handleEvent(const sf::Event&)
{
	return false;
}
