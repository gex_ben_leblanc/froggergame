#ifndef GEX_DATATABLES_H
#define GEX_DATATABLES_H

#include "ResourceIdentifiers.h"

#include "Particle.h"

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <vector>
#include <functional>


struct ParticleData
{
	sf::Color						color;
	sf::Time						lifetime;
};



std::map<Particle::Type, ParticleData>			initializeParticleData();

#endif