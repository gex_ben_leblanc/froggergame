#ifndef LISTENER_I_H
#define LISTENER_I_H

#include "Entity.h"
#include "GameEvents.h"

class ListenerI : public gex::Entity
{

public:
						ListenerI(){};
	virtual void		onEvent(GameEvent e) = 0; //abstract virtual class to reLoad the cannon
};

#endif