#include "GameState.h"
#include "StateID.h"
#include "MusicPlayer.h"


GameState::GameState(gex::StateStack& stack, gex::State::Context context) :
State(stack, context),
_world(*context.window, *context.fonts),
_player(*context.player)
{
	//context.music->play("../Media/Sounds/Circus_Theme.flac");
	context.music->setVolume(100);
}

GameState::~GameState()
{
	getContext().music->stop();
}

void GameState::draw()
{
	_world.draw();
}

bool GameState::update(sf::Time dt)
{
	//
	//update the game world
	//
	_world.update(dt);
	
	//
	//process realtime input
	//
	gex::CommandQueue& commands = _world.getCommandQueue();
	_player.handleRealtimeInput(commands);

	return true;
}

bool GameState::handleEvent(const sf::Event& event)
{
	// Game input handling
	gex::CommandQueue& commands = _world.getCommandQueue();
	_player.handleEvent(event, commands);

	// Escape pressed, trigger the pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
		requestStackPush(StateID::Pause);

	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::G)
		requestStackPush(StateID::Gex);

	return true;
}