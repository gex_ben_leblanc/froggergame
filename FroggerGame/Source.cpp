//
//
// sfml demo

#include <iostream>
#include <stdexcept>

#include "Application.h"

int main()
{
	try
	{
		Application app;
		app.run();
	}
	catch (std::exception& e)
	{
		std::cout << "\nException: " << e.what() << std::endl;
	}

}